-- vim: set ft=applescript:

-- main
tell application "iTunes"
    set all_albums to {}
    set the_result to {}
    -- do we have a selection, or not?
    if selection is not {} then
        -- selection is a list
        set the_library to selection
        set the_library_length to (count items of the_library)
        -- iterate over that list
        repeat with i from 1 to the_library_length
            try
                copy (get album of item i of the_library) to current_album
                -- do we have an album?
                if current_album is not "" then
                    -- was it collected before?
                    if all_albums does not contain current_album then
                        -- if no, then collect it
                        copy current_album to end of all_albums
                        -- and then proceed with gathering the requested information
                        -- in addition to the album, we collect artist...
                        if (get artist of item i of the_library) is not "" then
                            set current_artist to (get artist of item i of the_library)
                        else
                            set current_artist to ("UNKNOWN")
                        end if
                        -- ...and the year
                        if (get year of item i of the_library) is not 0 then
                            set current_year to (get year of item i of the_library)
                        else
                            set current_year to ("XXXX")
                        end if
                        -- now, store the data (see the helper below)
                        my save_to(current_artist & " - [" & current_year & "] " & current_album, the_result)
                        my save_to(return, the_result)
                    end if
                end if
            end try
        end repeat
        -- and send the saved data to the clipboard
        set the clipboard to (the_result as string)
    -- now, we have no seletion
    else
        -- and thus we do operate with an entire view
        set the_library to a reference to (get view of front window)
        -- get the number of tracks in that view
        set the_library_length to (count of every track in the_library)
        -- and iterate over those tracks (differently compared to iteration over a list)
        repeat with i from 1 to the_library_length
            try
                set current_track to (a reference to track i of the_library)
                copy (current_track's album as string) to current_album
                -- do we have an album?
                if current_album is not "" then
                    -- was it collected before?
                    if all_albums does not contain current_album then
                        -- if no, then collect it
                        copy current_album to end of all_albums
                        -- and then proceed with gathering the requested information
                        -- in addition to the album, we collect artist...
                        if (current_track's artist as string) is not "" then
                            set current_artist to (current_track's artist as string)
                        else
                            set current_artist to ("UNKNOWN")
                        end if
                        -- ...and the year
                        if (current_track's year as string) is not "0" then
                            set current_year to (current_track's year as string)
                        else
                            set current_year to ("XXXX")
                        end if
                        -- now, store the data (see the helper below)
                        my save_to(current_artist & " - [" & current_year & "] " & current_album, the_result)
                        my save_to(return, the_result)
                    end if
                end if
            end try
        end repeat
        -- and send the saved data to the clipboard
        set the clipboard to (the_result as string)
    end if
end tell
display dialog "Done!" giving up after 1

-- a helper to add text to a provided string
to save_to(x, y)
    copy x to end of y
end save_to
