# ArtistYearAlbum2Clipboard

## About

ArtistYearAlbum2Clipboard is a little Apple script for iTunes to copy album names of the current playlist (or selection) to clipboard (according to the following protocol: `Artist - [Year] Album`). The compiled applescript can be downloaded [here](https://bitbucket.org/dsjkvf/applescript-ayac/downloads/), while `ArtistYearAlbum2Clipboard.applescript` file in the repository is a plain text source file.

## Usage

Copy the [provided script](https://bitbucket.org/dsjkvf/applescript-ayac/downloads/ArtistYearAlbum2Clipboard.scpt) to `~/Library/iTunes/Scripts`, launch iTunes , and then just run the script from the Scripts menu.
